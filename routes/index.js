var express = require('express');
var router = express.Router();
var schema = require('../DbDetails.js');

router.get('/', function(req, res) {
	res.render('index');
});

router.post('/userlist',function(req,res){
	var Movie=schema.Movie;
	//get data
	var userName=req.body.tName;
	Movie.find({user:userName}).exec(function(err,docs){
		if(err){throw err;}
		console.log(docs);
		res.render('movielist',{login:userName,docs:docs});
	});
});

router.post('/adddata',function(req,res){
	var out=[];
	var Movie=schema.Movie;
	var msg=JSON.parse(req);
//	var msg={movie:[{year:'1998',rating:'2.5',imdb:'5.3',meta:'32',link:'http:\/\/imdb.com\/title\/tt0120685\/',name:'Godzilla'},
//	{year:'1994',rating:'9.5',imdb:'9.0',meta:'94',link:'http:\/\/imdb.com\/title\/tt0110912\/',name:'Pulp Fiction'},
//	{year:'2004',rating:'7.5',imdb:'8.1',meta:'86',link:'http:\/\/imdb.com\/title\/tt0405159\/',name:'Million Dollar Baby'}],
//	login:{password:'123',user:'karan'}};
	var user=msg.login.user;
	msg.movie.forEach(function(t){
		var addMovie=new Movie({user:user,
			name:t.name,
			year:t.year,
			imdb:t.imdb,
			meta:t.meta,
			rating:t.rating,
			link:t.link});
		addMovie.save(function(err,addMovie){
			//TODO-add error handling
		});
		console.log(t.name);
	});
	res.send(out);
});

module.exports = router;
/*
db.lists.insert({user:'karan',name:'robocop',year:1988,imdb:6.6,meta:7.6,rating:8,link:'/tt123'})
db.lists.insert({user:'karan',name:'labyrinth',year:1998,imdb:8.6,meta:8.6,rating:10,link:'/tt343'});
db.lists.insert({user:'karan',name:'teletubbies',year:1811,imdb:1.6,meta:2.6,rating:.5,link:'/tt653'});
db.lists.insert({user:'neha',name:'my little pony',year:2010,imdb:9.6,meta:7.6,rating:8,link:'/tt123'})
db.lists.insert({user:'neha',name:'hellraiser',year:1985,imdb:4.6,meta:8.6,rating:10,link:'/tt343'});
 */
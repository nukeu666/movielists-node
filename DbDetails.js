var mongoose=require('mongoose');
var movieSchema=mongoose.Schema({
		user:String,
		name:String,
		year:Number,
		imdb:Number,
		meta:Number,
		rating:Number,
		link:String
	});
var Movie=mongoose.model('lists',movieSchema);
exports.Movie=Movie;